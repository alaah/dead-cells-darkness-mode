![screenshot](https://steamuserimages-a.akamaihd.net/ugc/940572502838788059/7694B6668B8A48445D21B2BE432892EC22D10E7D/)

# Howto
This is suited for [barebones](https://gitlab.com/alaah/dead-cells-barebones).

# License
See LICENSE for information
Of course this is based on absolutely proprietary files. I consider my "changes" to be GPL. No one will care anyway.